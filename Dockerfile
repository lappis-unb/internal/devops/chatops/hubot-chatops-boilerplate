FROM node:8-alpine

ENV HUBOT_ADAPTER=rocketchat                                    \
    HUBOT_OWNER="LAPPIS UnB"                                    \
    HUBOT_NAME=hubot                                            \
    HUBOT_DESCRIPTION="A simple hubot bot"                      \
    HUBOT_LOG_LEVEL=debug                                       

WORKDIR hubot

ADD . /hubot

RUN apk --update add --no-cache --virtual build-dependencies    \
                     git make gcc g++ python                 && \
    npm install                                              && \
    apk del build-dependencies

ENTRYPOINT bin/hubot
